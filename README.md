Filter Tester
=============

A simple analog filter tester.

It creates a sine wave with ramping frequency and measures the output amplitude.
