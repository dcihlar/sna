import serial

__all__ = ['FiltTest', 'FiltTestError']


class FiltTestError(Exception):
	pass


class FiltTest(object):
	_gain_lut = (1, 10, 20, 30, 40, 60, 80, 120, 157, 0.2, 1)

	def __init__(self, dev = '/dev/ttyACM0', debug = False):
		self.s = serial.Serial(dev, 9600)
		self.debug = debug
		self.mode = None

	def _send_cmd(self, *cmd):
		cmd_str = ' '.join(str(e) for e in cmd)
		data = []
		self.s.write(cmd_str + '\n')
		if self.debug:
			print '>', cmd_str
		while True:
			ln = self.s.readline().strip()
			if self.debug:
				print '<', ln
			if ln == '':
				raise IOError('Connection broken')
			elif ln == 'OK':
				break
			elif ln == 'ERROR':
				raise FiltTestError('Command failed')
			data.append(ln)
		return data

	@staticmethod
	def _to_raw_freq(freq):
		return int(freq / 0.0596 + 0.5)

	@staticmethod
	def _from_raw_freq(raw_freq):
		return raw_freq * 0.0596

	# These methods convert raw ADC voltage to volts p2p.
	# Conversion formula depends on the selected readout mode.
	@staticmethod
	def _to_volts_unset_mode(raw_volts):
		raise RuntimeError('mode not set')
	@staticmethod
	def _to_volts_peak(raw_volts):
		return 2.56 * raw_volts / 1023
	@staticmethod
	def _to_volts_raw(raw_volts):
		return (5.0 * raw_volts / 1023 - 2.5) * 2
	_to_volts = _to_volts_unset_mode

	def hello(self):
		return self._send_cmd('H')

	def set_gain(self, gain, offset = 0):
		if gain not in self._gain_lut:
			raise ValueError('unsupported gain')
		gain = self._gain_lut.index(gain)
		self._send_cmd('G', gain, offset)

	def set_frequency(self, freq):
		self._send_cmd('FR', self._to_raw_freq(freq))

	def set_mode(self, mode, n_periods = 1):
		modes = ('raw', 'peak')
		if mode not in modes:
			raise ValueError('mode must be ' + ' or '.join(modes))
		self._send_cmd('M' + mode[0].upper(), n_periods)
		self.mode = mode
		self._to_volts = getattr(self, '_to_volts_' + mode)

	def get_mode(self):
		return self.mode

	def set_reset_mode(self, mode):
		modes = ('none', 'freqchg', 'read', 'wait')
		if mode not in modes:
			raise ValueError('reset mode must be ' + ' or '.join(modes))
		self._send_cmd('Z' + mode[0].upper())
		self.reset_mode = mode

	def get_reset_mode(self):
		return self.reset_mode

	def read(self):
		return self._to_volts(int(self._send_cmd('R')[0]))

	def scan(self, freq_start, freq_stop, freq_inc, n_settle_periods = 1):
		freq_start = self._to_raw_freq(freq_start)
		freq_stop  = self._to_raw_freq(freq_stop)
		freq_inc   = self._to_raw_freq(freq_inc)
		resp = self._send_cmd('SR', freq_start, freq_stop, freq_inc, n_settle_periods)
		resp = map(int, resp)
		return zip(
				map(self._from_raw_freq, range(freq_start, freq_stop, freq_inc)),
				map(self._to_volts, resp)
			)
