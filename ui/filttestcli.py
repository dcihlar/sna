#!/usr/bin/env python
from filttest import *
import csv


def main():
	ft = FiltTest()
	ft.set_gain(1)
	ft.set_mode('peak', 2)

	with open('out.csv', 'wt') as f:
		w = csv.writer(f)
		w.writerows(ft.scan(1000, 1000000, 100))


if __name__ == '__main__':
	main()
