#include <SPI.h>

#include "MAX9939.h"


MAX9939::MAX9939(uint8_t cs_pin) :
	cs_pin(cs_pin)
{
	digitalWrite(cs_pin, HIGH);
	pinMode(cs_pin, OUTPUT);
}

void MAX9939::reset() {
	SPI.begin();

	measuring = false;
	writeReg(Register::GAIN, 0);
	// shut down and set trim to 0
	writeData(0x80);
}

void MAX9939::writeData(uint8_t data) {
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(LSBFIRST);
	digitalWrite(cs_pin, LOW);
	SPI.transfer(data);
	digitalWrite(cs_pin, HIGH);
}

void MAX9939::writeReg(Register reg, uint8_t data) {
	data = (data << 1) & 0x3E;
	data |= measuring ? 0x40 : 0x00;
	data |= (reg == Register::GAIN) ? 0x01 : 0x00;
	writeData(data);
}

void MAX9939::enableMeasuring(bool measuring) {
	this->measuring = measuring;
}

void MAX9939::setGain(Gain gain) {
	writeReg(Register::GAIN, (uint8_t)gain);
}

void MAX9939::setTrim(int8_t trim) {
	bool neg = false;

	if (trim < 0) {
		trim = -trim;
		neg = true;
	}

	trim &= 0x0F;
	if (neg) trim |= 0x10;

	writeReg(Register::TRIM, trim);
}
