#ifndef _MAX9939_H_
#define _MAX9939_H_

#include <Arduino.h>

class MAX9939 {
	private:
		uint8_t cs_pin;
		bool measuring;

	public:
		enum Gain {
			GAIN1 = 0,
			GAIN10,
			GAIN20,
			GAIN30,
			GAIN40,
			GAIN60,
			GAIN80,
			GAIN120,
			GAIN157,
			GAIN02,
			GAIN1_ALT,
		};

		MAX9939(uint8_t cs_pin);
		
		void reset();
		void setGain(Gain gain);
		void setTrim(int8_t trim);
		void enableMeasuring(bool measuring);

	protected:
		enum Register { TRIM, GAIN };

		void writeData(uint8_t data);
		void writeReg(Register reg, uint8_t data);
};

#endif
