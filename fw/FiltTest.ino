#include <MiniGen.h>
#include <MAX9939.h>


const byte pin_led = 7;
const byte pin_pga_cs = 4;
const byte pin_minigen_cs = 2;
const byte pin_adc_peak = 4;
const byte pin_adc_raw = 11; // according to variants/leonardo/pins_arduino.h
const byte pin_discharge = A2;

static MiniGen dds(pin_minigen_cs);
static MAX9939 pga(pin_pga_cs);

#define RST_MODE_NONE           'N'
#define RST_MODE_FREQ_CHANGE    'F'
#define RST_MODE_READ           'R'
#define RST_MODE_WAIT           'W'

static unsigned int cfg_num_periods = 1;
static byte cfg_adc_pin = pin_adc_peak;
static char cfg_rst_mode = RST_MODE_FREQ_CHANGE;
static uint32_t cur_freq;


static void led_set(bool state) {
	digitalWrite(pin_led, state);
}

static void led_toggle() {
	digitalWrite(pin_led, !digitalRead(pin_led));
}

static void led_busy() {
	static unsigned long last_ts;
	if (millis() - last_ts > 200) {
		last_ts = millis();
		led_toggle();
	}
}

static void reset_peak() {
	pinMode(pin_discharge, OUTPUT);
	delayMicroseconds(4);
	pinMode(pin_discharge, INPUT);
}

static void set_mode(bool is_raw) {
	if (is_raw) {
		cfg_adc_pin = pin_adc_raw;
		analogReference(DEFAULT);
	} else {
		cfg_adc_pin = pin_adc_peak;
		analogReference(INTERNAL);
	}
}

static void set_frequency(uint32_t freq) {
	//TODO: clip to min freq! div/0 danger!
	cur_freq = freq;
	dds.adjustFreq(MiniGen::FREQ0, freq);
	if (cfg_rst_mode == RST_MODE_FREQ_CHANGE) {
		reset_peak();
	} else if (cfg_rst_mode == RST_MODE_WAIT) {
		// wait for self-discharge to take place
		// (worst case: from 5V to 2.5V at 80mV/6ms rate)
		delay(200);
	}
}

void setup() {
	Serial.begin(9600);
	pinMode(pin_led, OUTPUT);

	digitalWrite(pin_discharge, LOW);
	pinMode(pin_discharge, INPUT);

	set_mode(false);

	pga.reset();
	pga.setGain(MAX9939::Gain::GAIN02);

	dds.reset();
	dds.setMode(MiniGen::SINE);
	dds.setFreqAdjustMode(MiniGen::FULL);
	set_frequency(dds.freqCalc(1000));

	// we are ready to receive commands
	led_set(true);
}

static uint16_t calc_num_adcs_in_periods(uint16_t num_periods) {
	// The magic number is based on 120us per ADC read and
	// conversion from raw to normal frequency.
	const uint32_t n_adcs32 = 139821UL * num_periods / cur_freq + 1;
	return (n_adcs32 > 0xFFFE) ? 0xFFFE : n_adcs32;
}

static void delay_num_periods(uint16_t num_periods) {
	uint16_t num_delays = calc_num_adcs_in_periods(num_periods);
	while (num_delays--) {
		delayMicroseconds(120);
	}
}

static uint16_t read_peak() {
	const uint16_t n_reads = calc_num_adcs_in_periods(cfg_num_periods) + 1;
	uint16_t read_iter;
	uint16_t peak = 0;

	if ((cfg_adc_pin == pin_adc_peak) &&
	    (cfg_rst_mode == RST_MODE_READ)) {
		reset_peak();
	}

	for (read_iter = 0; read_iter < n_reads; ++read_iter) {
		uint16_t cur;
		cur = analogRead(cfg_adc_pin);
		if (cur > peak) peak = cur;
	}

	return peak;
}

static bool cmd_set_gain(const char *cfg) {
	int nargs, offset;
	unsigned int gain;

	nargs = sscanf_P(cfg, PSTR("%u %d"), &gain, &offset);

	if (nargs >= 1) {
		pga.setGain((MAX9939::Gain)gain);
	}

	if (nargs >= 2) {
		pga.setTrim(offset);
	}

	return nargs >= 1;
}

static bool cmd_set_frequency(const char *cfg) {
	bool raw = false;
	uint32_t freq;

	if (*cfg == 'R') {
		++cfg;
		raw = true;
	}

	if (sscanf_P(cfg, PSTR("%lu"), &freq) != 1)
		return false;

	if (!raw) {
		freq = dds.freqCalc(freq);
	}

	set_frequency(freq);

	return true;
}

static bool cmd_set_mode(const char *cfg) {
	unsigned int num_periods;
	int nargs;
	char mode;

	nargs = sscanf_P(cfg, PSTR("%c %u"), &mode, &num_periods);

	if (nargs >= 1) {
		if        (mode == 'R') {
			set_mode(true);
		} else if (mode == 'P') {
			set_mode(false);
		} else {
			return false;
		}
	}

	if (nargs >= 2) {
		cfg_num_periods = num_periods;
	}

	return nargs >= 1;
}

static bool cmd_reset_mode(const char *cfg) {
	int nargs;
	char mode;

	nargs = sscanf_P(cfg, PSTR("%c"), &mode);

	if (nargs >= 1) {
		if ((mode == RST_MODE_NONE) ||
		    (mode == RST_MODE_FREQ_CHANGE) ||
		    (mode == RST_MODE_READ) ||
		    (mode == RST_MODE_WAIT)) {
			cfg_rst_mode = mode;
		} else {
			return false;
		}
	}

	return nargs >= 1;
}


static bool cmd_read(const char *cfg) {
	bool inf;

	inf = *cfg == 'I';

	do {
		Serial.println(read_peak());

		if (Serial.available() > 0) {
			Serial.read();
			inf = false;
		}

		led_busy();

		delay(10);
	} while (inf);

	led_set(true);

	return true;
}

static bool cmd_hello(const char *cfg) {
	Serial.println(F("Hello!"));
	Serial.println(F("This is Filter Tester by DCihlar, 2017"));
	return true;
}

static bool cmd_scan(const char *cfg) {
	uint32_t freq, freq_start, freq_stop, freq_inc;
	uint16_t n_periods_to_settle = 1;
	bool raw = false;
	int nargs;

	if (*cfg == 'R') {
		++cfg;
		raw = true;
	}

	nargs = sscanf_P(cfg, PSTR("%lu %lu %lu %u"),
	                 &freq_start, &freq_stop, &freq_inc,
	                 &n_periods_to_settle);

	if (nargs < 3)
		return false;

	if (!raw) {
		freq_start = dds.freqCalc(freq_start);
		freq_stop  = dds.freqCalc(freq_stop);
		freq_inc   = dds.freqCalc(freq_inc);
	}

	if (freq_start > freq_stop ||
	    freq_start < 1 ||
	    freq_stop < 1 ||
	    freq_inc < 1)
	{
		return false;
	}

	for (freq = freq_start; freq < freq_stop; freq += freq_inc) {
		set_frequency(freq);
		delay_num_periods(n_periods_to_settle);
		Serial.println(read_peak());

		if (Serial.available() > 0) {
			Serial.read();
			break;
		}

		led_busy();
	}
	led_set(true);

	return true;
}

void loop() {
	static char line[81];
	int line_len;
	bool is_ok;

	if (Serial.available() > 0) {
		const char *args;
		line_len = Serial.readBytesUntil('\n', line, sizeof(line) - 1);
		line[line_len] = 0;

		if (!line_len)
			return;

		is_ok = false;
		args = line + 1;
		switch (*line) {
			case 'G':
				is_ok = cmd_set_gain(args);
				break;

			case 'F':
				is_ok = cmd_set_frequency(args);
				break;

			case 'M':
				is_ok = cmd_set_mode(args);
				break;

			case 'Z':
				is_ok = cmd_reset_mode(args);
				break;

			case 'R':
				is_ok = cmd_read(args);
				break;

			case 'H':
				is_ok = cmd_hello(args);
				break;

			case 'S':
				is_ok = cmd_scan(args);
				break;
		}

		Serial.println(is_ok ? F("OK") : F("ERROR"));
	}
}
